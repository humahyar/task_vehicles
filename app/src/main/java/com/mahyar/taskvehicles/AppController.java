package com.mahyar.taskvehicles;

import android.app.Application;

import com.mahyar.taskvehicles.di.component.AppComponent;
import com.mahyar.taskvehicles.di.component.DaggerAppComponent;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppController extends Application {

    public static AppController instance;
    private AppComponent appComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        appComponent= DaggerAppComponent.create();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSans(FaNum).ttf")
                .build());

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
