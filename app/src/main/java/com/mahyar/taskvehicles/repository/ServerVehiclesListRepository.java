package com.mahyar.taskvehicles.repository;

import com.mahyar.taskvehicles.api.VehiclesApi;
import com.mahyar.taskvehicles.data.retroModels.VehiclesListModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class ServerVehiclesListRepository {

    VehiclesApi api;
    @Inject
   public ServerVehiclesListRepository(VehiclesApi vehiclesApi){
       api=vehiclesApi;
   }

    public Single<VehiclesListModel> getVehiclesList(){
        return api.getVehiclesLis();
    }
}
