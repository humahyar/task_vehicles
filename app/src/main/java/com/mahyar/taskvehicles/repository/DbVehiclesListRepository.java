package com.mahyar.taskvehicles.repository;
import com.mahyar.taskvehicles.data.db.VehiclesDao;
import com.mahyar.taskvehicles.data.db.table.VehiclesTable;
import java.util.List;
import javax.inject.Inject;

public class DbVehiclesListRepository {
    private VehiclesDao dao;
    @Inject
    public DbVehiclesListRepository(VehiclesDao vehiclesDao){
        dao=vehiclesDao;
    }

    public void saveVehicles(List<VehiclesTable> vehiclesList){
        dao.saveVehicles(vehiclesList);
    }
    public void saveVehicles(VehiclesTable vehicles){
        dao.saveVehicles(vehicles);
    }
    public void updateVehicles(VehiclesTable  vehicles){
        dao.updateVehicles(vehicles);
    }
    public void deleteVehicles(VehiclesTable  vehicles){ dao.deleteVehicles(vehicles); }
    public void deleteVehiclesTable(){ dao.deleteVehiclesTable();}
    public List<VehiclesTable> getAllVehicles() {
        return dao.getAllVehicles();
    }
    public VehiclesTable getVehiclesWithId(int id) {
        return dao.getVehiclesWithId(id);
    }
}
