package com.mahyar.taskvehicles.di.component;

import com.mahyar.taskvehicles.di.module.AppModule;
import com.mahyar.taskvehicles.di.module.DbModule;
import com.mahyar.taskvehicles.di.module.NetworkModule;
import com.mahyar.taskvehicles.view.fragment.MapVehiclesFragment;
import com.mahyar.taskvehicles.view.fragment.VehiclesListFragment;

import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, AppModule.class, DbModule.class})
public interface AppComponent {
    void inject(VehiclesListFragment vehiclesListFragment);
    void inject(MapVehiclesFragment mapVehiclesFragment);
}