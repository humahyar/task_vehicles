package com.mahyar.taskvehicles.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mahyar.taskvehicles.di.annotation.ViewModelKey;
import com.mahyar.taskvehicles.viewmodels.HomeViewModel;
import com.mahyar.taskvehicles.viewmodels.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
@Module
public abstract class ViewModelModule {

    @Binds
    public abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);

    @IntoMap
    @Binds
    @ViewModelKey(HomeViewModel.class)
    public abstract ViewModel bindsHomeViewModel(HomeViewModel viewModel);
}
