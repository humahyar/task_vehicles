package com.mahyar.taskvehicles.di.module;


import com.mahyar.taskvehicles.AppController;
import com.mahyar.taskvehicles.data.db.RoomDB;
import com.mahyar.taskvehicles.data.db.VehiclesDao;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class DbModule {
    @Provides
    @Singleton
    static VehiclesDao provideVehicles() {
        RoomDB database = RoomDB.getDatabase(AppController.instance);
        return  database.getDao();
    }
}
