package com.mahyar.taskvehicles.di.module;

import com.mahyar.taskvehicles.api.VehiclesApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module(includes = ViewModelModule.class)
public abstract class NetworkModule {

    public static String BaseUrl = "https://snapp.ir";

    @Provides
    @Singleton
    static Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    static VehiclesApi provideVehicles(Retrofit retrofit) {
        return retrofit.create(VehiclesApi.class);

    }
}
