package com.mahyar.taskvehicles.util;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.content.ContextCompat;

import com.mahyar.taskvehicles.AppController;

public class PermissionUtil {

    public static boolean checkStorage() {
        int result = ContextCompat.checkSelfPermission(AppController.instance, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }else
            return false ;
    }
    public static boolean checkVersion() {
        if (Build.VERSION.SDK_INT >= 23)
            return true;
        else
            return false;

    }

}
