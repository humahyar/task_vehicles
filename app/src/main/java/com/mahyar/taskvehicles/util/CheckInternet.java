package com.mahyar.taskvehicles.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.mahyar.taskvehicles.AppController;

public class CheckInternet {

    public static boolean isConnected(){
            ConnectivityManager connectivity =
                    (ConnectivityManager) AppController.instance.
                            getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if (info != null && info.isConnected()) {

                    return true;
                }else
                    return false ;
            }
        return false;
    }
}
