package com.mahyar.taskvehicles.data.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mahyar.taskvehicles.data.db.table.VehiclesTable;

import java.util.List;

@Dao
public interface VehiclesDao {
    @Insert
    void saveVehicles(List<VehiclesTable> vehiclesList);
    @Insert
    void saveVehicles(VehiclesTable vehicles);
    @Update
    void updateVehicles(VehiclesTable vehicles);
    @Delete
    void deleteVehicles(VehiclesTable vehicles);
    @Query("DELETE FROM VehiclesTable")
    void deleteVehiclesTable();

    @Query("SELECT * FROM VehiclesTable")
    List<VehiclesTable> getAllVehicles();

    @Query("SELECT * FROM VehiclesTable WHERE id = :id")
    VehiclesTable getVehiclesWithId(int id);
}
