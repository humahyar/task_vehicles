package com.mahyar.taskvehicles.data.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mahyar.taskvehicles.data.db.table.VehiclesTable;

@Database(entities = {VehiclesTable.class}, version = 1)
public abstract class RoomDB extends RoomDatabase {
    public abstract  VehiclesDao getDao();

    public  static RoomDB getDatabase(Context context) {
        RoomDB database = Room.databaseBuilder(context, RoomDB.class, "db-vehicles-task")
                .allowMainThreadQueries()
                .build();
        return database ;
    }
}