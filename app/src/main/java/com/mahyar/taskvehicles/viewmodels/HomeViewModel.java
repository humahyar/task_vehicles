package com.mahyar.taskvehicles.viewmodels;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.mahyar.taskvehicles.data.db.table.VehiclesTable;
import com.mahyar.taskvehicles.data.retroModels.VehiclesListModel;
import com.mahyar.taskvehicles.data.retroModels.VehiclesModel;
import com.mahyar.taskvehicles.enums.CarType;
import com.mahyar.taskvehicles.repository.DbVehiclesListRepository;
import com.mahyar.taskvehicles.repository.ServerVehiclesListRepository;
import com.mahyar.taskvehicles.widget.vehicleslistwidget.VehiclesList;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends ViewModel {

    private  ServerVehiclesListRepository serverRepository;
    private  DbVehiclesListRepository dbRepository;
    private final CompositeDisposable disposable = new CompositeDisposable();
    public MutableLiveData<List<VehiclesTable>> liveVehiclesList = new MutableLiveData<>();
    @Inject
    public HomeViewModel(ServerVehiclesListRepository serverRepository,
                         DbVehiclesListRepository dbRepository){
        this.serverRepository=serverRepository;
        this.dbRepository=dbRepository;
    }

    public void getVehiclesList() {
        disposable.add(serverRepository.getVehiclesList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<VehiclesListModel>() {
                    @Override
                    public void onSuccess(VehiclesListModel list) {
                        typeConvertVehiclesList(list);
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d("error vehicles list", Objects.requireNonNull(e.getMessage()));
                    }
                }));
    }

    private void typeConvertVehiclesList(VehiclesListModel list){
        List<VehiclesTable>vehiclesList=new ArrayList<>();
        for(VehiclesModel vehicles:list.vehicles){
            VehiclesTable model=new VehiclesTable();
            model.setType(vehicles.type);
            model.setLat(vehicles.lat);
            model.setLng(vehicles.lng);
            model.setBearing(vehicles.bearing);
            model.setImage_url(vehicles.image_url);
            vehiclesList.add(model);
        }
        checkChangeDB(vehiclesList);
    }

    private void checkChangeDB(List<VehiclesTable>vehiclesList){
        boolean isChangeDB=false;
        if(vehiclesList.size()==dbRepository.getAllVehicles().size()) {
            for (int index=0;index<vehiclesList.size();index++){
                VehiclesTable apiList=vehiclesList.get(index);
                VehiclesTable dbList=dbRepository.getAllVehicles().get(index);
                if(!apiList.getType().equalsIgnoreCase(dbList.getType()) ||
                        !apiList.getImage_url().equalsIgnoreCase(dbList.getImage_url()) ||
                        apiList.getLat()!=dbList.getLat()||
                        apiList.getLng()!=dbList.getLng()||
                        apiList.getBearing()!=dbList.getBearing()){
                    isChangeDB=true;
                    break;
                }
            }
        }else
            isChangeDB=true;

        if(isChangeDB){
            deleteFolderImage();
            dbRepository.deleteVehiclesTable();
            dbRepository.saveVehicles(vehiclesList);
        }
        liveVehiclesList.postValue(dbRepository.getAllVehicles());

    }

    public void getListFromDB(){
        liveVehiclesList.postValue(dbRepository.getAllVehicles());
    }

    public void saveImageTable(Bitmap bitmapImage,int tableId){
        if (isExternalStorageWritable()) {
            saveImageVehiclesOnSdcard(bitmapImage,tableId);
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void saveImageVehiclesOnSdcard(Bitmap finalBitmap,int tableId) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.task_vehicles/");
        if (!myDir.exists())
            myDir.mkdirs();

        String fileName = tableId +".png";

        File file = new File(myDir, fileName);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path=root + "/.task_vehicles/"+fileName;
        updateTableWithImage(path,tableId);
    }

    private void deleteFolderImage(){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.task_vehicles/");
        if (myDir.isDirectory()) {
            String[] children = myDir.list();
            if (children != null) {
                for (String child : children) {
                    new File(myDir, child).delete();
                }
            }
        }
    }
    private void updateTableWithImage(String imagePath,int tableId){
        VehiclesTable model=dbRepository.getVehiclesWithId(tableId);
        model.setImage(imagePath);
        dbRepository.updateVehicles(model);
    }

    public List<VehiclesTable> getVehicleForMap(String idTable){
        List<VehiclesTable>list=new ArrayList<>();
        if(idTable.equalsIgnoreCase("all"))
            list.addAll(dbRepository.getAllVehicles());
        else
            list.add(dbRepository.getVehiclesWithId(Integer.parseInt(idTable)));
        return list;
    }

    public String getCarType(String type){
        CarType carType= CarType.getCarType(type);
      return carType.faName;
    }




}
