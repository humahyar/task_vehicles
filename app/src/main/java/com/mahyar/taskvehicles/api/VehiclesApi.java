package com.mahyar.taskvehicles.api;

import com.mahyar.taskvehicles.data.retroModels.VehiclesListModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface VehiclesApi {
    @GET("/assets/test/document.json")
    Single<VehiclesListModel> getVehiclesLis();
}
