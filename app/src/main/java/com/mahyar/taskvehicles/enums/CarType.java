package com.mahyar.taskvehicles.enums;

import com.mahyar.taskvehicles.AppController;
import com.mahyar.taskvehicles.R;

public enum CarType {
    ECO( 1, AppController.instance.getResources().getString(R.string.eco)),
    PLUS(2,AppController.instance.getResources().getString(R.string.plus)),
    ROSE(3,AppController.instance.getResources().getString(R.string.rose)),
    BIKE(4,AppController.instance.getResources().getString(R.string.bike)),
    DEFAULT(5,"");
    public int carTypeId;
    public String faName;

    CarType(int carTypeId,String faName){
        this.carTypeId=carTypeId;
        this.faName=faName;
    }

    public static CarType getCarType(String name) {
        switch (name) {
            case "ECO":
                return ECO;
            case "PLUS":
                return PLUS;
            case "ROSE":
                return ROSE;
            case "BIKE":
                return BIKE;
            default:
               return DEFAULT;
        }
    }

}
