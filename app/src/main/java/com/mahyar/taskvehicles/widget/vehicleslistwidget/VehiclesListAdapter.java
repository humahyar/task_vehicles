package com.mahyar.taskvehicles.widget.vehicleslistwidget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.mahyar.taskvehicles.R;
import com.mahyar.taskvehicles.data.db.table.VehiclesTable;
import com.mahyar.taskvehicles.databinding.ItemVehiclesListWidgetBinding;
import com.mahyar.taskvehicles.enums.CarType;
import com.mahyar.taskvehicles.util.CheckInternet;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.util.List;


public class VehiclesListAdapter extends RecyclerView.Adapter<VehiclesListAdapter.WidgetListViewHolder> {
    private List<VehiclesTable> listItem;
    private LayoutInflater layoutInflater;
    private VehiclesList.GetClickItemAdapter listener;
    private Context mContext;
    public VehiclesListAdapter(Context context, List<VehiclesTable> list) {
        this.listItem=list;
        this.mContext=context;
    }
    @Override
    public void onBindViewHolder(@NonNull final VehiclesListAdapter.WidgetListViewHolder holder, final int position) {
        try {
            VehiclesTable item = listItem.get(position);
            if (item != null)
                holder.itemBinding.setItem(item);
            if (position % 2 == 0)
                holder.itemBinding.parent.setBackgroundColor(mContext.getResources().getColor(R.color.colorGray));

            if (item.getImage()==null) {
                if(CheckInternet.isConnected()) {
                    Picasso.get()
                            .load(item.getImage_url())
                            .placeholder(R.drawable.place_holder)
                            .error(R.drawable.place_holder)
                            .into(holder.itemBinding.imgCar, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Bitmap image = ((BitmapDrawable) holder.itemBinding.imgCar.getDrawable()).getBitmap();
                                    listener.onReadyBitMapImage(item.getId(),image);
                                }
                                @Override
                                public void onError(Exception e) {
                                    listener.onReadyBitMapImage(item.getId(), null);
                                }
                            });
                }else
                    holder.itemBinding.imgCar.setImageResource(R.drawable.place_holder);
            } else {
                File file = new File(item.getImage());
                if (file.exists()) {
                    Bitmap image = BitmapFactory.decodeFile(item.getImage());
                    holder.itemBinding.imgCar.setImageBitmap(image);
                }else
                    holder.itemBinding.imgCar.setImageResource(R.drawable.place_holder);
            }

                CarType carType= CarType.getCarType(item.getType());
                holder.itemBinding.tvTypeCar.setText(carType.faName);

            holder.itemView.setOnClickListener(v -> {
                listener.onClickItem(item.getId());
            });
        }catch (Error| Exception e){
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public VehiclesListAdapter.WidgetListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());
            ItemVehiclesListWidgetBinding itemBinding = DataBindingUtil.inflate(layoutInflater,
                    R.layout.item_vehicles_list_widget, parent, false);
            return new WidgetListViewHolder(itemBinding);
    }

    public static class WidgetListViewHolder extends RecyclerView.ViewHolder {
        ItemVehiclesListWidgetBinding itemBinding;
        public WidgetListViewHolder(ItemVehiclesListWidgetBinding _itemBinding) {
            super(_itemBinding.getRoot());
            this.itemBinding = _itemBinding;
        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    @Override
    public int getItemViewType(int position) {
            return position;
    }

    public void setClickItemListener(VehiclesList.GetClickItemAdapter _listener){
        listener=_listener;
    }

}