package com.mahyar.taskvehicles.widget.vehicleslistwidget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mahyar.taskvehicles.R;
import com.mahyar.taskvehicles.data.db.table.VehiclesTable;
import com.mahyar.taskvehicles.widget.BaseWidget;

import java.util.List;

public class VehiclesList extends BaseWidget {
    private RecyclerView rvVehicles;
    private AppCompatTextView tvListIsEmpty;
    private ConstraintLayout layOnMap;
    private VehiclesListAdapter adapter;
    private Context mContext;
    private GetClickOnWidget listener;

    @BindingAdapter("liveVehiclesList")
    public static void setValue(VehiclesList view, List<VehiclesTable> _itemList) {
        view.addList(_itemList);
    }
    public VehiclesList(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext=context;
    }

    @Override
    public void initLayout(Context context, @Nullable AttributeSet attrs) {
        inflateLayout(context, R.layout.widget_vehicles_list);
        rvVehicles = view.findViewById(R.id.rvVehicles);
        tvListIsEmpty = view.findViewById(R.id.tvListIsEmpty);
        layOnMap = view.findViewById(R.id.layOnMap);
    }


    public void addList(List<VehiclesTable> itemList) {
        if (itemList != null) {
            if (!itemList.isEmpty()) {
                listFill();
                rvVehicles.setLayoutManager(new LinearLayoutManager(getContext()));
                adapter = new VehiclesListAdapter(mContext,itemList);
                rvVehicles.setAdapter(adapter);
                adapter.setClickItemListener(new GetClickItemAdapter() {
                    @Override
                    public void onClickItem(int id) {
                        listener.onClickList(id);
                    }

                    @Override
                    public void onReadyBitMapImage(int id, Bitmap image) {
                        listener.onReadyBitMapImage(id,image);
                    }
                });
                layOnMap.setOnClickListener(v -> listener.onClickMapIcon());
            } else
                showListIsEmpty();
        }
    }
    public void clickOnWidget(GetClickOnWidget _listener){
        listener=_listener;
    }
    public void showListIsEmpty() {
        tvListIsEmpty.setVisibility(View.VISIBLE);
        rvVehicles.setVisibility(View.GONE);
    }
    public void listFill() {
        tvListIsEmpty.setVisibility(View.GONE);
        rvVehicles.setVisibility(View.VISIBLE);
    }

    public interface GetClickItemAdapter {
        void onClickItem(int id);
        void onReadyBitMapImage(int id, Bitmap image);
    }

    public interface  GetClickOnWidget{
        void onClickList(int id);
        void onClickMapIcon();
        void onReadyBitMapImage(int id, Bitmap image);
    }
}
