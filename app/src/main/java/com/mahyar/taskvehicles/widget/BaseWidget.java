package com.mahyar.taskvehicles.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

public abstract class BaseWidget extends LinearLayout {
    protected LayoutInflater inflater;
    protected View view;

    public BaseWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setOrientation(VERTICAL);
        inflater = LayoutInflater.from(context);
        initLayout(context, attrs);
    }

    public abstract void initLayout(Context context, @Nullable AttributeSet attrs);
    protected void inflateLayout(Context context, @LayoutRes int layoutRes) {
        view = inflater.inflate(layoutRes, this);
    }

}
