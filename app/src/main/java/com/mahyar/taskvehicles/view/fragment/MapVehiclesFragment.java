package com.mahyar.taskvehicles.view.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mahyar.taskvehicles.AppController;
import com.mahyar.taskvehicles.R;
import com.mahyar.taskvehicles.data.db.table.VehiclesTable;
import com.mahyar.taskvehicles.databinding.FragmentVehiclesListBinding;
import com.mahyar.taskvehicles.viewmodels.HomeViewModel;

import java.io.File;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

public class MapVehiclesFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private HomeViewModel viewModel;
    private String vehiclesId ;
    private View view;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_map_vehicles, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        init();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setTrafficEnabled(true);
        addMarker(viewModel.getVehicleForMap(Objects.requireNonNull(vehiclesId)));
    }

    private void init(){
        ((AppController) getActivity().getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);
        getExtra();

    }

    private void getExtra() {
        Bundle bundle = this.getArguments();
        if (bundle != null)
            vehiclesId= bundle.getString("put");
    }

    @SuppressLint("DefaultLocale")
    private void addMarker(List<VehiclesTable> markerList){
        Bitmap image=null;
        for(VehiclesTable vehiclesModel:markerList) {
            LatLng carLocation = new LatLng(vehiclesModel.getLat(), vehiclesModel.getLng());
            MarkerOptions markerOptions = new MarkerOptions();
            File file = new File(vehiclesModel.getImage());
            if (file.exists())
                image = BitmapFactory.decodeFile(vehiclesModel.getImage());
            markerOptions.position(carLocation);
            markerOptions.title(viewModel.getCarType(vehiclesModel.getType()));
            Marker marker = mMap.addMarker(markerOptions);
            if(image!=null)
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(image));
            marker.setTag(vehiclesModel.getType());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(carLocation.latitude, carLocation.longitude))
                    .zoom(17)
                    .bearing(vehiclesModel.getBearing())
                    .tilt(30)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }
}