package com.mahyar.taskvehicles.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;
import com.mahyar.taskvehicles.R;

public class BaseFragment extends Fragment {
    protected final  int PERMISSIONS_STORAGE  = 101;
    public BaseFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void hideKeyboard() {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    public void handlePageDestination(View view,int id) {
        Navigation.findNavController(view).navigate(id);
    }
    public void handlePageDestinationWithData(View view, int id, Bundle bundle) {
        Navigation.findNavController(view).navigate(id,bundle);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    protected void showSnack(String message) {
        Activity activity = getActivity();
        if (activity != null) {
            try {
                Snackbar snackbar = Snackbar
                        .make(getActivity().findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundResource(R.color.colorBlack);
                TextView tv = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tv.setTextColor(getResources().getColor(R.color.colorWhite));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                else
                    tv.setGravity(Gravity.CENTER_HORIZONTAL);
                snackbar.show();
            } catch (Exception e) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

}
