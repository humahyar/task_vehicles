package com.mahyar.taskvehicles.view.fragment;

import android.Manifest;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import com.mahyar.taskvehicles.AppController;
import com.mahyar.taskvehicles.R;
import com.mahyar.taskvehicles.data.db.table.VehiclesTable;
import com.mahyar.taskvehicles.databinding.FragmentVehiclesListBinding;
import com.mahyar.taskvehicles.util.CheckInternet;
import com.mahyar.taskvehicles.util.PermissionUtil;
import com.mahyar.taskvehicles.viewmodels.HomeViewModel;
import com.mahyar.taskvehicles.widget.vehicleslistwidget.VehiclesList;

import javax.inject.Inject;

public class VehiclesListFragment extends BaseFragment {
    private View view;
    private HomeViewModel viewModel;
    private FragmentVehiclesListBinding viewBinding;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_vehicles_list, container, false);
        view = viewBinding.getRoot();
        init();
        return view;
    }
    private void init(){
        ((AppController) getActivity().getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);
        viewBinding.setViewModel(viewModel);
        viewBinding.setLifecycleOwner(this);
        if (PermissionUtil.checkVersion()) {
            if (!PermissionUtil.checkStorage()) {
                requestStorage();
            }else
                getList();
        }else
            getList();

    }
    private void requestStorage() {
        requestPermissions(new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_STORAGE);
    }

    private void getList(){
        if(CheckInternet.isConnected())
            viewModel.getVehiclesList();
        else {
            showSnack(getResources().getString(R.string.please_connected_internet));
            viewModel.getListFromDB();
        }

        viewBinding.vehiclesList.clickOnWidget(new VehiclesList.GetClickOnWidget() {
            @Override
            public void onClickList(int id) {
                checkAfterMap(Integer.toString(id));
            }

            @Override
            public void onClickMapIcon() {
                checkAfterMap("all");
            }

            @Override
            public void onReadyBitMapImage(int id, Bitmap image) {
                if (PermissionUtil.checkVersion()) {
                    if (PermissionUtil.checkStorage())
                        viewModel.saveImageTable(image,id);
                }else
                    viewModel.saveImageTable(image,id);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_STORAGE) {
            getList();
        }
    }

    private void checkAfterMap(String extera){
        if(CheckInternet.isConnected()) {
            Bundle bundle = new Bundle();
            bundle.putString("put",extera);
            handlePageDestinationWithData(view, R.id.action_vehiclesListFragment_to_mapVehiclesFragment,bundle);
        }else
            showSnack(getResources().getString(R.string.please_connected_internet));
    }
}
